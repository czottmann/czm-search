require 'rubygems'
require 'bundler'
require 'rack/rewrite'

use Rack::Static
use Rack::Rewrite do
  r301 %r{.+\bq=([^&$]*)}, 'https://duckduckgo.com/?q=!g+site:czm.io+$1'
end

run Proc.new { |env|
  [
    '301',
    {
      'Content-Type' => 'application/octet-stream',
      'Location' => 'https://czm.io',
    },
    []
  ]
}
